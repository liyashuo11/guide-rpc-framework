package github.javaguide.remoting.transport.socket.simple;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.Serializable;

/**
 * @Data 是一个组合注解
 */
@Data
@AllArgsConstructor
public class Message implements Serializable {

    private String content;
}
